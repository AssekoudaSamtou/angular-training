import * as express from "express"
import {Application} from "express"
import {getAllUsers, getUsersByEmail} from "./get-user.ctr"

const bodyParser = require('body-parser')

const app: Application = express()

app.use(bodyParser.json())

app.route('/api/users').get(getAllUsers)
app.route('/api/user').get(getUsersByEmail)

const httpServer: any = app.listen(3000, () =>
  console.log("Http Rest API running at http://localhost:" + httpServer.address().port))
